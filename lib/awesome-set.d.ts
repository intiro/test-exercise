export declare class AwesomeSet {
    private _values;
    private _added;
    constructor();
    add(value: string): void;
    remove(value: string): void;
    getValues(): string[];
}
