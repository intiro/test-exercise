export declare class ArrayUtil {
    private _flatten;
    flatten(arr: Array<any>): Array<any>;
    find(arr: Array<{
        [key: string]: any;
    }>, keys: {
        [key: string]: any;
    }): {
        [key: string]: any;
    } | null;
    range(from: number, to: number): Array<number>;
}
