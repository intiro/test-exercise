export declare class HttpUtil {
    expires: Date;
    getCacheControlHeader(isPublic: boolean, hours?: number, modifier?: string): string;
    private getExpirationDate;
    getExpiresHeader(hours?: number): string;
    parseQueryStringFromUrl(url: string): any;
}
