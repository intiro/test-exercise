export declare class StringUtil {
    reverse(input: string): string;
    capitalize(input: string): string;
    summarize(str: string, max: number): string;
    contains(str: string, strPart: string): boolean;
    underlinePrimaryName(name: string, primaryName: string): string;
    rotateLetters(str: string, n: number): string;
}
