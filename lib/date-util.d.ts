export declare class DateUtil {
    parseDate(dateStr: string): Date;
    addDays(date: Date, days: number): Date;
    addHours(date: Date, hours: number): Date;
}
